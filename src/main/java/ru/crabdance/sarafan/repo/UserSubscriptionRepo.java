package ru.crabdance.sarafan.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.crabdance.sarafan.domain.User;
import ru.crabdance.sarafan.domain.UserSubscription;
import ru.crabdance.sarafan.domain.UserSubscriptionId;

import java.util.List;

public interface UserSubscriptionRepo extends JpaRepository<UserSubscription, UserSubscriptionId> {
    List<UserSubscription> findBySubscriber(User user);
}
