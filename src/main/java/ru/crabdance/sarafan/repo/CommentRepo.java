package ru.crabdance.sarafan.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.crabdance.sarafan.domain.Comment;

public interface CommentRepo extends JpaRepository<Comment, Long> {
}
