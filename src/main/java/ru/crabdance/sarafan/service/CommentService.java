package ru.crabdance.sarafan.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.crabdance.sarafan.domain.Comment;
import ru.crabdance.sarafan.domain.User;
import ru.crabdance.sarafan.domain.Views;
import ru.crabdance.sarafan.dto.EventType;
import ru.crabdance.sarafan.dto.ObjectType;
import ru.crabdance.sarafan.repo.CommentRepo;
import ru.crabdance.sarafan.util.WsSender;

import java.util.function.BiConsumer;

@Service
public class CommentService {
    private final CommentRepo commentRepo;
    private final BiConsumer<EventType, Comment> wsSender;

    @Autowired
    public CommentService(CommentRepo commentRepo, WsSender wsSender) {
        this.commentRepo = commentRepo;
        this.wsSender = wsSender.getSender(ObjectType.COMMENT, Views.FullComment.class);
    }

    public Comment create(Comment comment, User user) {
        comment.setAuthor(user);
        Comment commentFromDb = commentRepo.save(comment);

        wsSender.accept(EventType.CREATE, commentFromDb);

        return commentFromDb;
    }

}
